package com.example.nft_server2;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CollectionRepository extends CrudRepository<Collection, Integer>{
    @Query("SELECT c FROM Collection c WHERE c.user_id = ?1")
    Iterable<Collection> findCollectionsById(Integer user_id);

//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE User u SET u.email=:email WHERE u.metamask_id=:metamask_id",nativeQuery = true)
//    int updateUser(@Param("metamask_id") Integer metamask_id, @Param("email") String email);
}
