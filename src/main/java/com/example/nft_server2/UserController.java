package com.example.nft_server2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // This means that this class is a Controller
@RequestMapping(path="/user") // This means URL's start with /user (after
public class UserController {
    @Autowired // This means to get the bean called userRepository
    private UserRepository userRepository;

    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody String addNewUser (@RequestParam String email, @RequestParam String metamask_id) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = new User();
        n.setMetamask_id(metamask_id);
        n.setEmail(email);
        n.setVerified(0);
        userRepository.save(n);
        return "Saved";//
    }

    @PostMapping(path="/update") // Map ONLY POST Requests
    public @ResponseBody String updateUser (@RequestParam String metamask_id, @RequestParam String email) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = userRepository.findUserByMetamask(metamask_id);
        if (n==null){
            return "No record found";
        }
        else{
            n.setEmail(email);
            userRepository.updateUser(metamask_id, email);
            return n.getEmail();//
        }

    }

    @PostMapping(path="/apply") // Map ONLY POST Requests
    public @ResponseBody String applyVerify (@RequestParam String metamask_id) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = userRepository.findUserByMetamask(metamask_id);

        if (n.getEmail().equals("")){
            return "Empty Email";
        }

        if (n==null) {
            return "No record found";
        }

        if (n.getVerified()==2){
            return "Already Verified";
        }

        if (n.getVerified()==1){
            return "Already Applied for Verification";
        }
        userRepository.userVerification(metamask_id,1);
        return "Applied for Verification";//


    }

    @PostMapping(path="/accept") // Map ONLY POST Requests
    public @ResponseBody String acceptVerify (@RequestParam String metamask_id) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = userRepository.findUserByMetamask(metamask_id);

        if (n.getEmail().equals("")){
            return "Empty Email";
        }

        if (n==null) {
            return "No record found";
        }

        if (n.getVerified()==2){
            return "Already Verified";
        }

        if (n.getVerified()==0){
            return "Not Applied for Verification";
        }
        userRepository.userVerification(metamask_id,2);
        return "User Verified";//

    }

    @PostMapping(path="/reject") // Map ONLY POST Requests
    public @ResponseBody String rejectVerify (@RequestParam String metamask_id) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = userRepository.findUserByMetamask(metamask_id);

        if (n.getEmail().equals("")){
            return "Empty Email";
        }

        if (n==null) {
            return "No record found";
        }

        if (n.getVerified()==2){
            return "Already Verified";
        }

        if (n.getVerified()==0){
            return "Not Applied for Verification";
        }
        userRepository.userVerification(metamask_id,0);
        return "User Rejected";//

    }
    @GetMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepository.findAll();
    }
}
