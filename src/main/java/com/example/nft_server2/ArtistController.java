package com.example.nft_server2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // This means that this class is a Controller
@RequestMapping(path="/artist") // This means URL's start with /user (after
public class ArtistController {

    @Autowired // This means to get the bean called userRepository
    private UserRepository userRepository;

    @Autowired
    private CollectionRepository collectionRepository;

    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody String addNewCollection (@RequestParam String metamask_id,
                                                  @RequestParam String name,
                                                  @RequestParam String description,
                                                  @RequestParam String web_url ) {

        User n = userRepository.findUserByMetamask(metamask_id);

        if (n.getVerified()!=2){
            return "User not Verified";
        }

        Collection c = new Collection();
        c.setUser_id(n.getUser_id());
        c.setName(name);
        c.setDescription(description);
        c.setJson_url("");  //When Launchpad comes this will be initialized.
        c.setWeb_url(web_url);
        c.setMinted(0); // when Launchpad comes this will be initialized.

        collectionRepository.save(c);
        return "Saved";//
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Collection> getAllCollections() {
        // This returns a JSON or XML with the users
        return collectionRepository.findAll();
    }

    @PostMapping(path="/my")
    public @ResponseBody Iterable<Collection> getMyCollections(@RequestParam String metamask_id) {
        // This returns a JSON or XML with the users
        User n = userRepository.findUserByMetamask(metamask_id);
        if (n==null){
            return null;
        }
        return collectionRepository.findCollectionsById(n.getUser_id());

    }
}
