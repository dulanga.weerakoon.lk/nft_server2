package com.example.nft_server2;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.nft_server2.User;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface UserRepository  extends CrudRepository<User, Integer>{
    @Query("SELECT u FROM User u WHERE u.metamask_id = ?1")
    User findUserByMetamask(String metamask_id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE User u SET u.email=:email WHERE u.metamask_id=:metamask_id",nativeQuery = true)
    int updateUser(@Param("metamask_id") String metamask_id, @Param("email") String email);

    @Modifying
    @Transactional
    @Query(value = "UPDATE User u SET u.verified=:verified WHERE u.metamask_id=:metamask_id",nativeQuery = true)
    int userVerification(@Param("metamask_id") String metamask_id, @Param("verified") Integer verified);
}
